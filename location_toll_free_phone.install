<?php

/**
 * @file
 *   Install, update and uninstall functions for the location_toll_free_phone module.
 */

/**
 * Implements hook_schema().
 */
function location_toll_free_phone_schema() {
  $schema['location_toll_free_phone'] = array(
    'description' => 'location_toll_free_phone.module {location} supplementary table.',
    'fields' => array(
      'lid' => array(
        'description' => '{location}.lid',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'toll_free_phone' => array(
        'description' => 'Toll Free Phone number',
        'type' => 'varchar',
        'length' => 31,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('lid'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function location_toll_free_phone_install() {
  // Change weight.
  db_update('system')
    ->fields(array(
      'weight' => 1,
    ))
    ->condition('name', 'location_toll_free_phone')
    ->condition('type', 'module')
    ->execute();
}

/**
 * Location 3.0 update 1.
 * Fix pgsql -- The table definition was broken.
 */
function location_toll_free_phone_update_5300() {
  if (!db_table_exists('location_toll_free_phone')) {
    $schema['location_toll_free_phone'] = array(
      'description' => 'location_toll_free_phone.module {location} supplementary table.',
      'fields' => array(
        'lid' => array(
          'description' => '{location}.lid',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
        'toll_free_phone' => array(
          'description' => 'Toll Free Phone number',
          'type' => 'varchar',
          'length' => 31,
          'not null' => TRUE,
          'default' => '',
        ),
      ),
      'primary key' => array('lid'),
    );
    db_create_table('location_toll_free_phone', $schema['location_toll_free_phone']);
  }
  else {
    // If the table WAS created (i.e. user manually fixed bug and reinstalled), g/c the postal_code column.
    if (db_field_exists('location_toll_free_phone', 'postal_code')) {
      db_drop_field('location_toll_free_phone', 'postal_code');
    }
  }
}

/**
 * Location 3.0 update 2.
 * Change weight of module.
 */
function location_toll_free_phone_update_5301() {
  // This update was moved to update 5302.
}

/**
 * Location 3.0 update 2.
 * Change weight of module.
 */
function location_toll_free_phone_update_5302() {
  // Change weight.
  db_update('system')
    ->fields(array(
      'weight' => 1,
    ))
    ->condition('name', 'location_toll_free_phone')
    ->condition('type', 'module')
    ->execute();
}

/**
 * Drupal 6 updates.
 */
function location_toll_free_phone_update_6301() {
  db_change_field('location_toll_free_phone', 'location_toll_free_phone', 'location_toll_free_phone', array(
    'description' => 'Toll Free Phone number',
    'type' => 'varchar',
    'length' => 31,
    'not null' => TRUE,
    'default' => '',
  ));
}
